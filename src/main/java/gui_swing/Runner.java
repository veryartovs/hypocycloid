package gui_swing;

import gui_swing.ui.controller.MainFrameController;

import javax.swing.*;

public class Runner {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainFrameController controller = new MainFrameController();
                controller.showMainFrameWindow();
            }
        });
    }
}
