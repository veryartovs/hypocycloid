package gui_swing.ui.view;

import javax.swing.*;

public class MainFrame extends JFrame {
    private static final int WIDTH = 1024;
    private static final int HEIGHT = 768;
    private JPanel mainPanel;
    private HypocycloidPanel chart;
    private JSlider ratioSlider;
    private JLabel ratioLabel;
    private JButton clearButton;
    private JSlider animationSpeedSlider;
    private JButton startButton;

    public MainFrame() {
        setSize(WIDTH, HEIGHT);
        setContentPane(mainPanel);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
    }

    public JSlider getRatioSlider(){
        return ratioSlider;
    }

    public JLabel getRatioLabel(){
        return ratioLabel;
    }

    public HypocycloidPanel getChart(){
        return chart;
    }

    public JButton getClearButton() {
        return clearButton;
    }

    public JSlider getAnimationSpeedSlider() {
        return animationSpeedSlider;
    }

    public JButton getStartButton(){
        return startButton;
    }
}
