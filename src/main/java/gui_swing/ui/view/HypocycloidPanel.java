package gui_swing.ui.view;

import math.Hypocycloid;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class HypocycloidPanel extends JPanel {
    private Hypocycloid hc;
    private Timer timer;
    private int animationSpeed;
    private double currentDeg;
    private double step;
    private ArrayList<Point> points;
    private Point last_point;
    private Point2D.Double innerCircleCenter;
    private boolean isClosed;


    HypocycloidPanel(){
        setBackground(Color.WHITE);
        hc = new Hypocycloid(300, 100);

        currentDeg = 0;
        step = 5;
        animationSpeed = 10;

        points = new ArrayList<>();
        timer = new Timer(40, new TimerActionListener());

        innerCircleCenter = new Point2D.Double();
        isClosed = false;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawOuterCircle(g2d);
        if (!points.isEmpty()) {
            drawHypocycloidsLines(g2d);
            drawInnerCircle(g2d);
            drawLine(g2d);
        }
    }

    private void drawOuterCircle(Graphics2D g2d){
        g2d.setColor(Color.black);
        drawCenteredCircle(g2d, getWidth()/2f, getHeight()/2f, hc.getOuterCircleRadius());
    }

    private void drawInnerCircle(Graphics2D g2d) {
        g2d.setColor(Color.black);
        double innerR = hc.getInnerCircleRadius();
        double outerR = hc.getOuterCircleRadius();
        innerCircleCenter.x = getWidth()/2f + (outerR-innerR)*Math.cos(Math.toRadians(-currentDeg));
        innerCircleCenter.y = getHeight()/2f - (outerR-innerR)*Math.sin(Math.toRadians(-currentDeg));

        drawCenteredCircle(g2d, innerCircleCenter.x, innerCircleCenter.y, innerR);
    }

    private void drawCenteredCircle(Graphics2D g2d, double x, double y, double r) {
        Ellipse2D.Double circle = new Ellipse2D.Double(x-r,y-r,2*r,2*r);
        g2d.draw(circle);
    }

    private void drawLine(Graphics2D g2d) {
        Line2D.Double line = new Line2D.Double(innerCircleCenter.x, innerCircleCenter.y, last_point.x, last_point.y);
        g2d.draw(line);
        g2d.fill(new Ellipse2D.Double(innerCircleCenter.x-2, innerCircleCenter.y-2, 4, 4));
        g2d.setColor(Color.red);
        g2d.fill(new Ellipse2D.Double(last_point.x-4, last_point.y-4, 8, 8));
    }

    private void drawHypocycloidsLines(Graphics2D g2d) {
        g2d.setColor(Color.red);

        for (int i = 1; i < points.size(); i++){
            g2d.drawLine(points.get(i-1).x, points.get(i-1).y,
                    points.get(i).x, points.get(i).y);
        }
    }

    public void setRatio(double ratio){
        hc.setInnerCircleRadius(hc.getOuterCircleRadius()/ratio);
    }

    public void setAnimationSpeed(int speed){
        animationSpeed = speed;
    }

    public void startAnimation(){
        timer.start();
    }

    public void stopAnimation(){
        timer.stop();
    }

    public void clear(){
        points.clear();
        currentDeg = currentDeg % 360;
        isClosed = false;
        repaint();
    }

    public boolean isRunning(){
        return timer.isRunning();
    }

    private class TimerActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (!isClosed) {
                for (int i = 0; i < animationSpeed; i++) {
                    currentDeg += step;
                    points.add(nextPoint());
                    if (points.size() > 70 && points.get(0).equals(points.get(points.size()-1))){
                        isClosed = true;
                        break;
                    }
                }
            } else {
                currentDeg += step*animationSpeed;
            }
            last_point = nextPoint();
            repaint();
        }

        private Point nextPoint(){
            Point2D.Double pointToDraw = hc.calculatePointByDeg(currentDeg);
            pointToDraw.x += getWidth() / 2f;
            pointToDraw.y += getHeight() / 2f;
            return new Point((int) Math.round(pointToDraw.x),(int) Math.round(pointToDraw.y));
        }
    }
}
