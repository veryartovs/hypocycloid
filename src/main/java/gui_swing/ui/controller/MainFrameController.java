package gui_swing.ui.controller;

import gui_swing.ui.view.HypocycloidPanel;
import gui_swing.ui.view.MainFrame;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrameController {
    private MainFrame mainFrame;
    private JSlider ratioSlider;
    private JLabel ratioLabel;
    private JButton clearButton;
    private HypocycloidPanel chart;
    private JSlider animationSpeedSlider;
    private JButton startButton;

    public MainFrameController() {
        initComponents();
        initListeners();
    }

    public void showMainFrameWindow() {
        mainFrame.setVisible(true);
    }

    private void initComponents() {
        mainFrame = new MainFrame();
        ratioSlider = mainFrame.getRatioSlider();
        ratioLabel = mainFrame.getRatioLabel();
        clearButton = mainFrame.getClearButton();
        chart = mainFrame.getChart();
        animationSpeedSlider = mainFrame.getAnimationSpeedSlider();
        startButton = mainFrame.getStartButton();

        chart.setAnimationSpeed(animationSpeedSlider.getValue());
        updateRatioLabel();
    }

    private void initListeners() {
        ratioSlider.addChangeListener(new RatioSliderListener());
        clearButton.addActionListener(new ClearButtonListener());
        animationSpeedSlider.addChangeListener(new AnamationSpeedSliderListener());
        startButton.addActionListener(new StartButtonListener());
    }

    private void updateRatioLabel(){
        ratioLabel.setText("R/r = " + ratioSlider.getValue()/10f);
    }

    private class RatioSliderListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            updateRatioLabel();
            chart.setRatio(ratioSlider.getValue()/10f);
            chart.clear();
        }
    }

    private class ClearButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            chart.clear();
        }
    }

    private class AnamationSpeedSliderListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            chart.setAnimationSpeed(animationSpeedSlider.getValue());
        }
    }

    private class StartButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (!chart.isRunning()){
                startButton.setText("Stop");
                chart.startAnimation();
            } else {
                startButton.setText("Start");
                chart.stopAnimation();
            }
        }
    }
}