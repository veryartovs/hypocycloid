package math;

import java.awt.geom.Point2D;

public class Hypocycloid {

    private double outerCircleRadius;
    private double innerCircleRadius;
    private double radiusRatio;

    public Hypocycloid(double outerCircleRadius, double innerCircleRadius){
        if (innerCircleRadius == 0 || outerCircleRadius == 0) {
            throw new IllegalArgumentException("arguments can't be zero");
        }
        if (outerCircleRadius < innerCircleRadius){
            throw new IllegalArgumentException("outerCircleRadius must be bigger than innerCircleRadius");
        }
        this.outerCircleRadius = outerCircleRadius;
        this.innerCircleRadius = innerCircleRadius;
        updateRadiusRatio();
    }

    private void updateRadiusRatio(){
        this.radiusRatio = outerCircleRadius / innerCircleRadius;
    }

    public double getOuterCircleRadius(){
        return outerCircleRadius;
    }

    public double getInnerCircleRadius(){
        return innerCircleRadius;
    }

    public void setOuterCircleRadius(double outerCircleRadius) {
        if (innerCircleRadius == 0) {
            throw new IllegalArgumentException("outerCircleRadius can't be zero");
        }
        if (outerCircleRadius < innerCircleRadius){
            throw new IllegalArgumentException("outerCircleRadius must be bigger than innerCircleRadius");
        }
        this.outerCircleRadius = outerCircleRadius;
        updateRadiusRatio();
    }

    public void setInnerCircleRadius(double innerCircleRadius){
        if (innerCircleRadius == 0) {
            throw new IllegalArgumentException("innerCircleRadius can't be zero");
        }
        if (outerCircleRadius < innerCircleRadius){
            throw new IllegalArgumentException("outerCircleRadius must be bigger than innerCircleRadius");
        }
        this.innerCircleRadius = innerCircleRadius;
        updateRadiusRatio();
    }

    public Point2D.Double calculatePointByDeg(double deg){
        return calculatePointByRad(Math.toRadians(deg));
    }

    public double calculateYByDeg(double deg){
        return calculateYByRad(Math.toRadians(deg));
    }

    public double calculateXByDeg(double deg){
        return calculateXByRad(Math.toRadians(deg));
    }

    public Point2D.Double calculatePointByRad(double rad){
        return new Point2D.Double(calculateXByRad(rad), calculateYByRad(rad));
    }

    public double calculateXByRad(double rad){
        return Math.cos(rad)*(radiusRatio-1)*innerCircleRadius + Math.cos((radiusRatio-1)*rad)*innerCircleRadius;
    }

    public double calculateYByRad(double rad){
        return Math.sin(rad)*(radiusRatio-1)*innerCircleRadius - Math.sin((radiusRatio-1)*rad)*innerCircleRadius;
    }
}
