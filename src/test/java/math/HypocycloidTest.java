package math;

import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;

public class HypocycloidTest {
    @Test
    public void calculatePoint() {
        Hypocycloid hc = new Hypocycloid(4,1); // Astroid: k=4
        assertTrue(hc.calculatePointByDeg(0).equals(new Point2D.Double(4,0)));
        assertTrue(hc.calculatePointByDeg(90).equals(new Point2D.Double(0,4)));
        assertTrue(hc.calculatePointByDeg(180).equals(new Point2D.Double(-4,0)));
        assertTrue(hc.calculatePointByDeg(270).equals(new Point2D.Double(0,-4)));
    }
}